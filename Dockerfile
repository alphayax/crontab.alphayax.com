FROM alphayax/angular-build AS builder
WORKDIR /app
COPY crontab .
RUN npm install \
 && ng build


FROM httpd:2.4
LABEL maintainer="Alphayax <alphayax@gmail.com>"
COPY --from=builder /app/dist/crontab /usr/local/apache2/htdocs/
EXPOSE 80
