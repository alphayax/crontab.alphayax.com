
export class CrontabLog {
    isEnabled: boolean = false;
    isToAppend: boolean = false;
    logFile: string = '';
}
