import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommandSelectorComponent } from './command-selector.component';

describe('CommandSelectorComponent', () => {
  let component: CommandSelectorComponent;
  let fixture: ComponentFixture<CommandSelectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommandSelectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommandSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
